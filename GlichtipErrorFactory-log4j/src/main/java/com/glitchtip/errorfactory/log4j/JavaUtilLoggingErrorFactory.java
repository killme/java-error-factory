package com.glitchtip.errorfactory.log4j;

import java.util.logging.Level;
import java.util.logging.Logger;

public class JavaUtilLoggingErrorFactory {
    private static final Logger log = Logger.getLogger(
            JavaUtilLoggingErrorFactory.class.getName());

    public static void run() {
        logException();
    }

    private static void logException() {
        log.log(Level.SEVERE, "Dummy logged error",
                new RuntimeException("Error"));
    }
}
